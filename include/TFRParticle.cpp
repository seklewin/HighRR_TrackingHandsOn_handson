/*!
 *  @author    Alessio Piucci
 *  @brief     Representation of MC particles
 */

#include "TFRParticle.h"

ClassImp(TFRParticle)

//------------
// Constructor with vertex, momentum, charge and propagator
//------------
TFRParticle::TFRParticle(TVector3 _vertex, TVector3 _momentum,
			 double mass, double charge){
  
  this->fV.Set(_vertex);
  this->fP.Set(_momentum);
  this->fSign = charge;

  vertex = _vertex;
  momentum = _momentum;
  
  lorentz_vect.SetPxPyPzE(momentum[0], momentum[1], momentum[2],
			  sqrt(pow(momentum.Mag(), 2.) + pow(mass, 2.)));

  clusters = new TFRClusters();
  hits = new TFRMCHits();
  
  /*
  //initialize the vectors of hits and clusters of the particle
  hits = new TFMCRHits();
  clusters = new TFRClusters();
  */
  
}

//----------
// Add the hit to the particle vector of hits
//----------
void TFRParticle::AddHit(TFRMCHit *hit){

  //if the hit is valid, add it to the hits of the particle
  if(hit->GetPosition()[0] > -9999.)
    hits->Add(hit);
  
  return;
}

//----------
// Add the cluster to the particle vector of clusters
//----------
void TFRParticle::AddCluster(TFRCluster *cluster){

  //if the cluster is valid, add it to the clusters of the particle
  if(cluster->GetZ() > -9999.)
    clusters->Add(cluster);
  
  return;
}

